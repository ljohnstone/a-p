/* */ 
"use strict";
var attachComments = require('./attachComments');
var convertComments = require('./convertComments');
var toTokens = require('./toTokens');
var toAST = require('./toAST');
module.exports = function(ast, traverse, tt, code) {
  ast.tokens.pop();
  ast.tokens = toTokens(ast.tokens, tt, code);
  convertComments(ast.comments);
  toAST(ast, traverse, code);
  ast.type = "Program";
  ast.sourceType = ast.program.sourceType;
  ast.directives = ast.program.directives;
  ast.body = ast.program.body;
  delete ast.program;
  delete ast._paths;
  attachComments(ast, ast.comments, ast.tokens);
};
