import {StageComponent} from 'aurelia-testing';
import {bootstrap} from 'aurelia-bootstrapper';

describe('MyComponent', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources('src/sky-search-control')
      .inView('<sky-search-control text-field-text.bind="textFieldText" button-text.bind="buttonText"></sky-search-control>')
      .boundTo({ textFieldText: 'search text', buttonText: 'search' });
  });

  it('should render controls corectly', done => {
    component.create(bootstrap).then(() => {
      const inpuElement = document.querySelector('#sky-search-control-search');
      const buttonElement = document.querySelector('#sky-search-control-button');      
      const labelElement = document.querySelector('#sky-search-control-label'); 
      expect(inpuElement).not.toBe(null);
      expect(inpuElement.type).toBe('text');    
      expect(buttonElement).not.toBe(null);
      expect(buttonElement.type).toBe('submit');
      expect(buttonElement.innerHTML).toBe('search');
      expect(labelElement).not.toBe(null);      
      expect(labelElement.innerHTML).toBe('search text');      
      done();
    }).catch(e => { console.log(e.toString()) });
  });

  it('should aaaaaaaaaa', done => {
    component.create(bootstrap).then(() => {
      const inpuElement = document.querySelector('#sky-search-control-search');
      const buttonElement = document.querySelector('#sky-search-control-button');  
      inpuElement.value = 'john'
      console.log(inpuElement.value)

      var spyEvent = spyOnEvent('#sky-search-control-button', 'click')
      buttonElement.click()
      expect('click').toHaveBeenTriggeredOn('#sky-search-control-button')
    
      done();
    }).catch(e => { console.log(e.toString()) });
  });

  afterEach(() => {
    component.dispose();
  });
});