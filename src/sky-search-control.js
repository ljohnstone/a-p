import { inject, customElement, bindable } from 'aurelia-framework';

@inject(Element)
export class SkySearchControl {
  // @bindable textfieldText = 'search text';
  // @bindable buttonText = 'Search';  
  @bindable textFieldText = 'search text';
  @bindable buttonText = 'Search';    
  searchText = '';

  constructor(element) {
    this.element = element;
  }
  
  search() {
    var event = new CustomEvent('search', { 
      detail: this.searchText,
      bubbles: true
    });
    
    this.element.dispatchEvent(event);
  }
  
}