var _dec, _dec2, _dec3, _dec4, _class, _desc, _value, _class2, _descriptor, _descriptor2, _descriptor3;

function _initDefineProp(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
  });
}

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

function _initializerWarningHelper(descriptor, context) {
  throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
}

import { inject, bindable, bindingMode, TaskQueue, DOM } from 'aurelia-framework';

import { MDCRippleFoundation } from '@material/ripple';

export let MdcRippleCustomAttribute = (_dec = inject(Element), _dec2 = bindable({ defaultBindingMode: bindingMode.oneWay, defaultValue: null }), _dec3 = bindable({ defaultBindingMode: bindingMode.oneTime, defaultValue: null }), _dec4 = bindable({ defaultBindingMode: bindingMode.oneTime, defaultValue: null }), _dec(_class = (_class2 = class MdcRippleCustomAttribute {

  constructor(Element) {
    _initDefineProp(this, 'unbounded', _descriptor, this);

    _initDefineProp(this, 'activate', _descriptor2, this);

    _initDefineProp(this, 'deactivate', _descriptor3, this);

    const BROWSERSUPPORTSCSSVARS = this.browserSupportsCssVars();

    this.taskQueue = TaskQueue;

    this.rippleFoundation = new MDCRippleFoundation({
      browserSupportsCssVars: () => {
        return BROWSERSUPPORTSCSSVARS;
      },
      isUnbounded: function () {
        return this.unbounded;
      }.bind(this),
      isSurfaceActive: () => {
        return Element === DOM.activeElement;
      },
      addClass: Element.classList.add.bind(Element.classList),
      removeClass: Element.classList.remove.bind(Element.classList),
      registerInteractionHandler: Element.addEventListener.bind(Element),
      deregisterInteractionHandler: Element.removeEventListener.bind(Element),
      registerResizeHandler: window.addEventListener.bind(window, 'resize'),
      deregisterResizeHandle: window.removeEventListener.bind(window, 'resize'),
      updateCssVariable: Element.style.setProperty.bind(Element.style),
      computeBoundingRect: Element.getBoundingClientRect.bind(Element),
      getWindowPageOffset: () => {
        return { x: window.pageXOffset, y: window.pageYOffset };
      }
    });
  }

  browserSupportsCssVars() {
    const supportsFunctionPresent = window.CSS && typeof window.CSS.supports === 'function';
    if (!supportsFunctionPresent) {
      return;
    };
    const explicitlySupportsCssVars = window.CSS.supports('--css-vars', 'yes');
    const weAreFeatureDetectingSafari10plus = window.CSS.supports('(--css-vars: yes)') && window.CSS.supports('color', '#00000000');
    return explicitlySupportsCssVars || weAreFeatureDetectingSafari10plus;
  }

  bind(context, extContext) {
    if (this.activate) {
      context[this.activate] = this.taskQueue.queueMicroTask.bind(this.taskQueue, this.rippleFoundation.activate.bind(this.rippleFoundation));
    };
    if (this.deactivate) {
      context[this.deactivate] = this.taskQueue.queueMicroTask.bind(this.taskQueue, this.rippleFoundation.deactivate.bind(this.rippleFoundation));
    };
  }

  attached() {
    this.rippleFoundation.init();
  }

  detached() {
    this.rippleFoundation.destroy();
  }
}, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, 'unbounded', [_dec2], {
  enumerable: true,
  initializer: null
}), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, 'activate', [_dec3], {
  enumerable: true,
  initializer: null
}), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, 'deactivate', [_dec4], {
  enumerable: true,
  initializer: null
})), _class2)) || _class);;