'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SkySearchControl = undefined;

var _dec, _class, _desc, _value, _class2, _descriptor, _descriptor2;

var _aureliaFramework = require('aurelia-framework');

function _initDefineProp(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
  });
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

function _initializerWarningHelper(descriptor, context) {
  throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
}

var SkySearchControl = exports.SkySearchControl = (_dec = (0, _aureliaFramework.inject)(Element), _dec(_class = (_class2 = function () {
  function SkySearchControl(element) {
    _classCallCheck(this, SkySearchControl);

    _initDefineProp(this, 'textFieldText', _descriptor, this);

    _initDefineProp(this, 'buttonText', _descriptor2, this);

    this.searchText = '';

    this.element = element;
  }

  SkySearchControl.prototype.search = function search() {
    var event = new CustomEvent('search', {
      detail: this.searchText,
      bubbles: true
    });

    this.element.dispatchEvent(event);
  };

  return SkySearchControl;
}(), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, 'textFieldText', [_aureliaFramework.bindable], {
  enumerable: true,
  initializer: function initializer() {
    return 'search text';
  }
}), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, 'buttonText', [_aureliaFramework.bindable], {
  enumerable: true,
  initializer: function initializer() {
    return 'Search';
  }
})), _class2)) || _class);