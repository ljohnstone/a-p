'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MdcTextfieldCustomAttribute = undefined;

var _dec, _dec2, _class, _desc, _value, _class2, _descriptor;

var _aureliaFramework = require('aurelia-framework');

var _textfield = require('@material/textfield');

function _initDefineProp(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
  });
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

function _initializerWarningHelper(descriptor, context) {
  throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
}

var MdcTextfieldCustomAttribute = exports.MdcTextfieldCustomAttribute = (_dec = (0, _aureliaFramework.inject)(Element), _dec2 = (0, _aureliaFramework.bindable)({ defaultBindingMode: _aureliaFramework.bindingMode.oneWay, defaultValue: null }), _dec(_class = (_class2 = function () {
  function MdcTextfieldCustomAttribute(Element) {
    _classCallCheck(this, MdcTextfieldCustomAttribute);

    _initDefineProp(this, 'disabled', _descriptor, this);

    var LABEL = Element.querySelector(".mdc-textfield__label");
    var INPUT = Element.querySelector(".mdc-textfield__input");
    var HELP = INPUT.hasAttribute('aria-controls') ? document.getElementById(INPUT.getAttribute('aria-controls')) : null;
    var DUMMY = function DUMMY() {};

    this.textfieldFoundation = new _textfield.MDCTextfieldFoundation({
      addClass: Element.classList.add.bind(Element.classList),
      removeClass: Element.classList.remove.bind(Element.classList),
      addClassToLabel: LABEL ? LABEL.classList.add.bind(LABEL.classList) : DUMMY,
      removeClassFromLabel: LABEL ? LABEL.classList.remove.bind(LABEL.classList) : DUMMY,
      addClassToHelptext: HELP ? HELP.classList.add.bind(HELP.classList) : DUMMY,
      removeClassFromHelptext: HELP ? HELP.classList.remove.bind(HELP.classList) : DUMMY,
      helptextHasClass: HELP ? HELP.classList.remove.bind(HELP.classList) : function () {
        return false;
      },
      setHelptextAttr: HELP ? HELP.setAttribute.bind(HELP) : DUMMY,
      removeHelptextAttr: HELP ? HELP.removeAttribute.bind(HELP) : DUMMY,
      registerInputFocusHandler: INPUT.addEventListener.bind(INPUT, 'focus'),
      registerInputBlurHandler: INPUT.addEventListener.bind(INPUT, 'blur'),
      registerInputInputHandler: INPUT.addEventListener.bind(INPUT, 'input'),
      registerInputKeydownHandler: INPUT.addEventListener.bind(INPUT, 'keydown'),
      deregisterInputFocusHandler: INPUT.removeEventListener.bind(INPUT, 'focus'),
      deregisterInputBlurHandler: INPUT.removeEventListener.bind(INPUT, 'blur'),
      deregisterInputInputHandler: INPUT.removeEventListener.bind(INPUT, 'input'),
      deregisterInputKeydownHandler: INPUT.removeEventListener.bind(INPUT, 'keydown'),
      getNativeInput: function getNativeInput() {
        return INPUT;
      }
    });
  }

  MdcTextfieldCustomAttribute.prototype.attached = function attached() {
    this.textfieldFoundation.init();
  };

  MdcTextfieldCustomAttribute.prototype.detached = function detached() {
    this.textfieldFoundation.destroy();
  };

  MdcTextfieldCustomAttribute.prototype.disabledChanged = function disabledChanged(newVal, oldVal) {
    this.checkboxFoundation.setDisabled(newVal);
  };

  return MdcTextfieldCustomAttribute;
}(), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, 'disabled', [_dec2], {
  enumerable: true,
  initializer: null
})), _class2)) || _class);
;